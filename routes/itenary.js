var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

/* GET home page. */
router.get('/', function(req, res, next) {

	var count = req.query.count;
	console.log(count)
	var limit = 0;

	if(count){
		limit = Number(count);
	}

	Itenary.find({}).limit(limit).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {
				res.status(200).json(itenaries);
			} else {
				res.status(404).json({
					err: 'No itenaries found'
				});
			}
		}
	})
});


router.post('/', function(req, res, next) {
	
	var data = req.body;

	var newItenary = new Itenary(data);

	newItenary.save(function(err, savedItenary) {
		if (err) {
			res.status(500).json(err);
		} else {
			res.status(200).json(savedItenary);
		}
	})

});


router.get('/:itenaryId', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);
	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {
				res.status(200).json(itenaries[0]);
			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});

router.delete('/:itenaryId', function(req, res, next) {
	
	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);

	Itenary.find({
		_id: itenaryId
	}).remove().exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			res.status(200).json(itenaries);
		}
	})
});

router.post('/:itenaryId/days', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);

	var day_data = req.body;

	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {

				var day_list = [];

				day_list = itenaries[0].day_schema.map(function(dayObj) { return dayObj.day_num });

				for (var i = day_data.length - 1; i >= 0; i--) {
					if (day_list.indexOf(day_data[i].day_num) > -1) {
						return res.status(400).json({'err': 'Duplicate days added'});
					} else {
						itenaries[0].day_schema.push(day_data[i]);
						itenaries[0].save(function(itenarySaveError, savedItenary) {
							if (itenarySaveError) {
								res.status(500).json(itenarySaveError);
							} else {
								res.status(200).json(savedItenary);
							}
						})
					}
				}

			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});

router.delete('/:itenaryId/days/:dayId', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);
	var dayId = req.params.dayId;

	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {

				var day_id_list = [];

				day_id_list = itenaries[0].day_schema.map(function(dayObj) { return dayObj._id });

				var dayIndex = day_id_list.indexOf(dayId);

				if (dayIndex > -1) {
					itenaries[0].day_schema.splice(dayIndex, 1);
					itenaries[0].save(function(itenarySaveError, savedItenary) {
						if (itenarySaveError) {
							res.status(500).json(itenarySaveError);
						} else {
							res.status(200).json(savedItenary);
						}
					})
				} else {
					return res.status(404).json({err: 'Invalid Day ID'});
				}
				

			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});


router.put('/:itenaryId/intro', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);

	var intro_data = req.body;

	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {

				if (intro_data.description) {
					itenaries[0].introduction.description = intro_data.description;
				}

				if (intro_data.image_url) {
					itenaries[0].introduction.image_url = intro_data.image_url;
				}

				if (intro_data.other_desc) {
					itenaries[0].introduction.other_desc = intro_data.other_desc;
				}

				itenaries[0].save(function(itenarySaveError, savedItenary) {
					if (itenarySaveError) {
						res.status(500).json(itenarySaveError);
					} else {
						res.status(201).json(savedItenary);
					}
				});

			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});

router.post('/:itenaryId/description', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);

	var desc_data = req.body;

	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {

				var desc_list = [];

				desc_list = itenaries[0].introduction.other_desc.map(function(descObj) { return descObj.heading });

				for (var i = desc_data.length - 1; i >= 0; i--) {
					if (day_list.indexOf(desc_data[i].day_num) > -1) {
						return res.status(400).json({'err': 'Duplicate heading added'});
					} else {
						itenaries[0].introduction.other_desc.push(desc_data[i]);
						itenaries[0].save(function(itenarySaveError, savedItenary) {
							if (itenarySaveError) {
								res.status(500).json(itenarySaveError);
							} else {
								res.status(200).json(savedItenary);
							}
						})
					}
				}

			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});

router.delete('/:itenaryId/description/:descId', function(req, res, next) {

	var id = req.params.itenaryId

	var itenaryId = mongoose.Types.ObjectId(id);
	var descId = req.params.descId;

	Itenary.find({
		_id: itenaryId
	}).exec(function(err, itenaries) {
		if (err) {
			res.status(500).json(err);
		} else {
			if (itenaries.length > 0) {

				var desc_id_list = [];

				desc_id_list = itenaries[0].introduction.other_desc.map(function(descObj) { return descObj._id });

				var descIndex = desc_id_list.indexOf(descId);

				if (dayIndex > -1) {
					itenaries[0].introduction.other_desc.splice(descIndex, 1);
					itenaries[0].save(function(itenarySaveError, savedItenary) {
						if (itenarySaveError) {
							res.status(500).json(itenarySaveError);
						} else {
							res.status(200).json(savedItenary);
						}
					})
				} else {
					return res.status(404).json({err: 'Invalid Day ID'});
				}
				

			} else {
				res.status(404).json({
					err: 'Invalid Itenary ID'
				});
			}
		}
	})
});

module.exports = router;
