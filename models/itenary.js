var mongoose = require('mongoose');

var itenarySchema = new mongoose.Schema({
	locale:{
		type: String, // String for now later will be user object
		required: true
	},
	title : {
		type: String,
		required: true
	},
	location: {
		type: String,
		required: true
	},
	country: {
		type: String,
		required: true
	},
	introduction: {
		description: {
			type: String,
			required: true
		},
		image_url: {
			type: String,
			required: true
		},
		other_desc: [{
			heading: String,
			desc: String	// this can be text or html
		}]
	},
	day_schema: [{
		day_num: {
			type: Number,
			required: true
		},
		day_title: {
			type: String,
			required: true
		},
		location: {
			type: String,
			required: true
		},
		description: {
			type: String,
			required: true
		}
	}]

});

module.exports = mongoose.model('Itenary', itenarySchema);