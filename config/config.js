var mongoose = require('mongoose');

module.exports = function() {

	var connection_url = 'mongodb://localhost:27017';

	// Connecting the mongo database
	mongoose.connect(connection_url, function(error) {
		if (error) {
			console.log('Error connecting to mongo: ' + error);
		} else {
			console.log('Mongo is UP!')
		}
	});

}